# Бревно изменений

## [692](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release691...release692): 14 апреля 2021 21:27+05:00
### Фичи
- [C-12833](https://yt.skbkontur.ru/issue/C-12833) Виджет списка сертов. Релиз с отображением портальных
### Задачи
- [C-12852](https://yt.skbkontur.ru/issue/C-12852) Конвертить DSS с Сертума
- [C-12886](https://yt.skbkontur.ru/issue/C-12886) Добавить причину отклонения для фото
- [C-12997](https://yt.skbkontur.ru/issue/C-12997) В письмах о продлении отправлять все железные КЭКи на аутх
- [C-6100](https://yt.skbkontur.ru/issue/C-6100) Парсинг электронной выписки с nalog.ru
### Фиксы багов
- [C-13003](https://yt.skbkontur.ru/issue/C-13003) Тюнинг парсера адреса


## [691](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release690...release691): 12 апреля 2021 21:15+05:00
### Задачи
- [C-13034](https://yt.skbkontur.ru/issue/C-13034) Вернуть логгирование тела запроса в СС
- [C-13037](https://yt.skbkontur.ru/issue/C-13037) JsonValueProvider для WebApi вместо JsonBinder


## [690](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release689...release690): 9 апреля 2021 21:43+05:00
### Задачи
- [C-13001](https://yt.skbkontur.ru/issue/C-13001) Неизвестное значение X перечисления WizardStepType
### Фиксы багов
- [C-13030](https://yt.skbkontur.ru/issue/C-13030) Unknown TemplateId у интеграторов CC


## [689](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release688...release689): 8 апреля 2021 21:56+05:00
### Фичи
- [C-12258](https://yt.skbkontur.ru/issue/C-12258) Распознавание данных паспорта в ЛК
### Задачи
- [C-12856](https://yt.skbkontur.ru/issue/C-12856) Доработать визуализацию ЭП в заявлении
### Фиксы багов
- [C-13013](https://yt.skbkontur.ru/issue/C-13013) Ошибки после релиза WebApi в CC


## [688](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release687...release688): 8 апреля 2021 14:11+05:00
### Задачи
- [C-11510](https://yt.skbkontur.ru/issue/C-11510) Переезд CC на Web API
- [C-12980](https://yt.skbkontur.ru/issue/C-12980) Субботник. Варнинги при билде. март 2021


## [687](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release686...release687): 
### Задачи
- [C-12895](https://yt.skbkontur.ru/issue/C-12895) ДЕДЛАЙН 15 АПР. Лишние уведомления для оНЭП
- [C-12975](https://yt.skbkontur.ru/issue/C-12975) Субботник. TODO. март 2021


## [686](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release685...release686): 
### Задачи
- [C-12411](https://yt.skbkontur.ru/issue/C-12411) Добавить структурированное логирование в логирование ошибок API
- [C-12653](https://yt.skbkontur.ru/issue/C-12653) Выводить в памятке для визита инфу про СЦ
- [C-12817](https://yt.skbkontur.ru/issue/C-12817) Поправить шаг "Что предстоит после релиза подписания
- [C-12900](https://yt.skbkontur.ru/issue/C-12900) Исправить формат времени в копии сертификата
- [C-12991](https://yt.skbkontur.ru/issue/C-12991) Выделить отдельный инстанс PkiService на Staging для EasyCert


## [685](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release684...release685): 
### Задачи
- [C-12294](https://yt.skbkontur.ru/issue/C-12294) Превью страничек
- [C-12295](https://yt.skbkontur.ru/issue/C-12295) Модный скролл
- [C-12695](https://yt.skbkontur.ru/issue/C-12695) Обновить конфигурацию Hangfire до 1.7
- [C-12725](https://yt.skbkontur.ru/issue/C-12725) Кусочек тестов на РП ЛК
- [C-12783](https://yt.skbkontur.ru/issue/C-12783) Сборка здорового человека
- [C-12847](https://yt.skbkontur.ru/issue/C-12847) Перейти на новую версию метода change
- [C-12849](https://yt.skbkontur.ru/issue/C-12849) Валидировать reply-to перед отправкой письма
- [C-12889](https://yt.skbkontur.ru/issue/C-12889) 404 если не найден контроллер по пути /api
- [C-12920](https://yt.skbkontur.ru/issue/C-12920) Фикс сборки пакетов для окто


## [684](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release683...release684): 
### Задачи
- [C-12835](https://yt.skbkontur.ru/issue/C-12835) Не получается отправить клон на проверку, если у шаблона есть фото обратившегося


## [683](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release682...release683): 
### Задачи
- [C-11500](https://yt.skbkontur.ru/issue/C-11500) Встроить центр поддержки УКС


## [682.1](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release682...release682.1): 
### Задачи
- [C-12893](https://yt.skbkontur.ru/issue/C-12893) Добавить проверку значений на null при проверке данных организации


## [682](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release681...release682): 
### Задачи
- [C-12064](https://yt.skbkontur.ru/issue/C-12064) Регистрация в ЕСИА от разных принципалов
- [C-12732](https://yt.skbkontur.ru/issue/C-12732) Учитывать длину полей при обновлении данных организации


## [681](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release680...release681): 
### Фичи
- [C-11995](https://yt.skbkontur.ru/issue/C-11995) Реакт ЛК. Мастер выпуска.


## [680](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release679...release680): 
### Задачи
- [C-12097](https://yt.skbkontur.ru/issue/C-12097) Указание способа идентификации в сертификате
- [C-12838](https://yt.skbkontur.ru/issue/C-12838) Оптимизация запроса выборки данных для конкретной формы в ЛК
- [C-12529](https://yt.skbkontur.ru/issue/C-12529) Выпилить метод УЛ + отправки на проверку
- [C-12532](https://yt.skbkontur.ru/issue/C-12532) Писать сообщения и проставлять УЛ от пользователя CC


## [679](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release678...release679): 
### Задачи
- [C-12766](https://yt.skbkontur.ru/issue/C-12766) Правильно вычитывать аббревиатуры при проверке данных организации
- [C-12782](https://yt.skbkontur.ru/issue/C-12782) Убрать остатки логики SharedStatic из nuke при сборке армов
- [C-12805](https://yt.skbkontur.ru/issue/C-12805) Строка не найдена или изменена при отправке запроса в СМЭВ
- [C-12808](https://yt.skbkontur.ru/issue/C-12808) Добавить в API СС информацию об ошибки копирования документов
- [C-12827](https://yt.skbkontur.ru/issue/C-12827) Пофиксить логирование при невалидном серте в подписании
- [C-12633](https://yt.skbkontur.ru/issue/C-12633) FileStorageSyncTask разваливается из-за Concurrent modification
- [C-12652](https://yt.skbkontur.ru/issue/C-12652) При авторелизе форм получаем CME
### Фиксы багов
- [C-12830](https://yt.skbkontur.ru/issue/C-12830) Пофиксить кейс смены фамилии в подписании


## [678](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release677...release678): 
### Задачи
- [C-12693](https://yt.skbkontur.ru/issue/C-12693) Создание форм на ЕГАИС в CC для Екей
- [C-12780](https://yt.skbkontur.ru/issue/C-12780) Оптимизировать построение списка сертов в ЛК
- [C-12828](https://yt.skbkontur.ru/issue/C-12828) Поменять текст для баннера ОСТ


## [677](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release676...release677): 
### Фичи
- [C-12199](https://yt.skbkontur.ru/issue/C-12199) Подписание РЕАЛЬНО любым сертом
### Задачи
- [C-12811](https://yt.skbkontur.ru/issue/C-12811) Дать доступ Докси к методам CC для получения инфы о форме и выпущенном серте
- [C-12814](https://yt.skbkontur.ru/issue/C-12814) Починить вычисление причины и продление для ФЛ/ИП


## [676](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release675...release676): 
### Задачи
- [C-9657](https://yt.skbkontur.ru/issue/C-9657) Включить серверные трассировки в asp.net mvc
- [C-12485](https://yt.skbkontur.ru/issue/C-12485) Заполнять localityName (Город) правильно
- [C-12527](https://yt.skbkontur.ru/issue/C-12527) Выпилить место печати из заявления


## [675](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release674...release675): 
### Задачи
- [C-12643](https://yt.skbkontur.ru/issue/C-12643) Поднять версию C# до 9.0 в Cabinet


## [674](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release673...release674): 
### Задачи
- [C-12046](https://yt.skbkontur.ru/issue/C-12046) Показывать кота сразу в опросах
- [C-12477](https://yt.skbkontur.ru/issue/C-12477) Разрешить редактировать реквизиты выписки, подписанной ФНС
- [C-12779](https://yt.skbkontur.ru/issue/C-12779) Проблемы с установкой серта на карту Rosan и Esmart


## [673](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release672...release673): 
### Задачи
- [C-12772](https://yt.skbkontur.ru/issue/C-12772) Собрать логов про ошибку запроса к кастомизации
- [C-12777](https://yt.skbkontur.ru/issue/C-12777) Обновить версию компонента certificate-list


## [672](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release671...release672): 
### Задачи
- [C-12518](https://yt.skbkontur.ru/issue/C-12518) Автоодобрение форм Екей
- [C-12606](https://yt.skbkontur.ru/issue/C-12606) Проанализировать статистику использования индексов базы Cabinet
- [C-12651](https://yt.skbkontur.ru/issue/C-12651) Авторелиз после автоодобрения для Екей
- [C-12660](https://yt.skbkontur.ru/issue/C-12660) Выпилить логику ковидных паспортов
- [C-12679](https://yt.skbkontur.ru/issue/C-12679) Пофиксить развес с Id сообщения в sql-запроса CC/Events
- [C-12738](https://yt.skbkontur.ru/issue/C-12738) Ошибка при скачивании счёта
### Фиксы багов
- [C-12446](https://yt.skbkontur.ru/issue/C-12446) При развале отправки сообщений к HistoryService не проставляется releasedate у выпущенного сертификата
- [C-12621](https://yt.skbkontur.ru/issue/C-12621) Обрабатывать развалы history сервиса при помечании на отзыв


## [671](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release670...release671): 
### Задачи
- [C-12669](https://yt.skbkontur.ru/issue/C-12669) Дать интегратору доступ к методам API
- [C-12757](https://yt.skbkontur.ru/issue/C-12757) Правильно сравнивать данные в req_forms и InnFLRequestInfos
- [C-12758](https://yt.skbkontur.ru/issue/C-12758) Починить регистрацию в ЕСИА для ФЛ


## [670](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release669...release670): 
### Задачи
- [C-12743](https://yt.skbkontur.ru/issue/C-12743) Не ждать ответа адаптера СМЭВ в кеширующем сервисе
- [C-12747](https://yt.skbkontur.ru/issue/C-12747) Правильно сравнивать данные в req_forms и PfrRequestInfos


## [669](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release668...release669): 
### Задачи
- [C-12071](https://yt.skbkontur.ru/issue/C-12071) Обновлять LastChange при манипуляциях в формой с BB
- [C-12113](https://yt.skbkontur.ru/issue/C-12113) Требования к безопасности myDSS
- [C-12640](https://yt.skbkontur.ru/issue/C-12640) Весь цикл выпуска облачных НЭПов через API
### Фиксы багов
- [C-12703](https://yt.skbkontur.ru/issue/C-12703) Вернуть на место шапку с анимацией при переходе между шагами


## [668](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release667...release668): 
### Фичи
- [C-10297](https://yt.skbkontur.ru/issue/C-10297) Перевод логики Кабинета на работу с кэширующим сервисом (Релиз 2)
### Задачи
- [C-12377](https://yt.skbkontur.ru/issue/C-12377) Доделки после релиза кэширующего сервиса


## [667](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release666...release667): 
### Задачи
- [C-12452](https://yt.skbkontur.ru/issue/C-12452) Скрытые причины отклонения не скрываются в СА
- [C-12639](https://yt.skbkontur.ru/issue/C-12639) дать доступ в АПИ CC для нового проекта Контура
- [C-12662](https://yt.skbkontur.ru/issue/C-12662) Оптимизировать построение списка сертов в ЛК
### Фиксы багов
- [C-12716](https://yt.skbkontur.ru/issue/C-12716) Едут повёрнутые картинки при открытии сайдпейджа


## [666](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release665...release666): 
### Фичи
- [C-12119](https://yt.skbkontur.ru/issue/C-12119) Фотографирование в CA
### Задачи
- [C-12158](https://yt.skbkontur.ru/issue/C-12158) Press F to pay respect to SharedStatic
- [C-12293](https://yt.skbkontur.ru/issue/C-12293) Переделать модалку в сайдпейдж
- [C-12328](https://yt.skbkontur.ru/issue/C-12328) Отображение названий доков в ЛК
- [C-12467](https://yt.skbkontur.ru/issue/C-12467) Агрегация ReqFormWorkflowException исключения
- [C-7738](https://yt.skbkontur.ru/issue/C-7738) Добавить кнопку "Удалить всё" в лайтбокс документа в ЛК
- [C-11406](https://yt.skbkontur.ru/issue/C-11406) Растащить FileUploadLightbox на компоненты
- [C-12300](https://yt.skbkontur.ru/issue/C-12300) Кнопка для увеличения скана


## [665](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release664...release665): 
### Задачи
- [C-12597](https://yt.skbkontur.ru/issue/C-12597) Обновить Hangfire в TaskService до версии 1.7


## [664](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release663...release664): 
### Задачи
- [C-9729](https://yt.skbkontur.ru/issue/C-9729) Обрабатывать ситуацию, когда мы выпустили сертификат, но не смогли его зарегать в Dss или Клауде
- [C-12484](https://yt.skbkontur.ru/issue/C-12484) Unable to cast object of type 'SKBKontur.Cabinet.Core.Logic.CADomains.Revoke.ErrorDataExtended' to type 'SKBKontur.Cabinet.Core.Logic.CADomains.Revoke.ErrorData'.
- [C-12656](https://yt.skbkontur.ru/issue/C-12656) При возвращении из ЛК не перерисовывается контрол УЛ
- [C-12675](https://yt.skbkontur.ru/issue/C-12675) Не разрывать телефон
- [C-12379](https://yt.skbkontur.ru/issue/C-12379) Прибрать набор полей в текущем контракте списка сертов
- [C-12626](https://yt.skbkontur.ru/issue/C-12626) Техрелиз: использование портального компонента список сертов в ЛК


## [663](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release662...release663): 
### Задачи
- [C-11663](https://yt.skbkontur.ru/issue/C-11663) Поправить тултип из вопросика про срок действия паспорта
- [C-11953](https://yt.skbkontur.ru/issue/C-11953) Запилить скачивание счета в TS
- [C-12567](https://yt.skbkontur.ru/issue/C-12567) Тултип с адресом из ФНС
- [C-12644](https://yt.skbkontur.ru/issue/C-12644) Дубли в BusinessTaskOperationFails
- [C-12657](https://yt.skbkontur.ru/issue/C-12657) Long query в ленте новостей по формам
- [C-12339](https://yt.skbkontur.ru/issue/C-12339) Long Query Detected пропущенные индексы
### Фиксы багов
- [C-11100](https://yt.skbkontur.ru/issue/C-11100) Ложные Long query detected


## [662](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release661...release662): 
### Задачи
- [C-11332](https://yt.skbkontur.ru/issue/C-11332) Удалить класс SKBKontur.Cabinet.CommonPackages.IntegrationTestUtils.Helpers.ExternalMailHelper
- [C-12496](https://yt.skbkontur.ru/issue/C-12496) В методах получения инфы о формах возвращать все типы форм
- [C-12569](https://yt.skbkontur.ru/issue/C-12569) Показывать время работы точки СЦ
- [C-12593](https://yt.skbkontur.ru/issue/C-12593) Выкинуть новый год из RC
- [C-12623](https://yt.skbkontur.ru/issue/C-12623) Выпилить Новый год 2021 из ЛК
- [C-12654](https://yt.skbkontur.ru/issue/C-12654) Пофиксить отправку метрик РП ЛК
- [C-12346](https://yt.skbkontur.ru/issue/C-12346) Борьба с аномалией два сертификата на одну форму


## [661](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release660...release661): 
### Задачи
- [C-12219](https://yt.skbkontur.ru/issue/C-12219) При входе на визард заполнения заявки перед шагом "что делать" мигает заголовок визарда
- [C-12326](https://yt.skbkontur.ru/issue/C-12326) Не скрывать ЕГРЮЛ УК, если РР и нетолько
- [C-12631](https://yt.skbkontur.ru/issue/C-12631) Авторелиз не должен разваливаться при отсутствие ответа по паспорту от смэв


## [660](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release659...release660): 
### Задачи
- [C-12416](https://yt.skbkontur.ru/issue/C-12416) Отпала точка — есть ускоренные в очереди
- [C-12520](https://yt.skbkontur.ru/issue/C-12520) При выпуске формы закрывать задачу не только лишь под сервисным пользователем
- [C-12622](https://yt.skbkontur.ru/issue/C-12622) Включить регистрацию портальных учёток не только для Эльбы
### Фиксы багов
- [C-12011](https://yt.skbkontur.ru/issue/C-12011) Спецэффекты с реквизитами паспорта и ValidTo


## [655](https://git.skbkontur.ru/cacabinet/cabinet/cabinet/compare/release654...release655): 
### Задачи
- [C-12507](https://yt.skbkontur.ru/issue/C-12507) Оптимизировать построение списка сертов в ЛК

