#Ключи Vault: наличие и необходимость

<table>
    <tr>
        <th></th>
        <th>ExternalStaging</th>
        <th>Study</th>
        <th>Kontur</th>
        <th>Megafon**</th>
        <th>Belgorod</th>
        <th>Practic</th>
        <th>Yamal</th>
    </tr>
    <tr>
        <th colspan="8">Секрет кабинета</th>
    </tr>
    <tr>
        <td>FocusApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>+</td>
        <td>+</td>
        <td>+</td>
        <td>+</td>
        <td>+</td>
    </tr>
    <tr>
        <td>PSPortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>+</td>
        <td>фейк</td>
        <td>фейк</td>
        <td>фейк</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <td>RAOpenIdConnectApiKey</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>+</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
    </tr>
    <tr>
        <td>SpanAggregationApiKey*</td>
        <td>-</td>
        <td>-</td>
        <td>+</td>
        <td>фейк подложили файлик</td>
        <td>фейк</td>
        <td>фейк</td>
        <td>фейк</td>
    </tr>
    <tr>
        <td>BillingAuthToken*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>+</td>
        <td>фейк</td>
        <td>фейк</td>
        <td>пустой</td>
        <td>пустой</td>
    </tr>
    <tr>
        <td>HerculesApiKey</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
        <td>+</td>
        <td>-</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
    </tr>
    <tr>
        <td>ELKApiKey*</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
        <td>пустое значение</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
    </tr>
    <tr>
        <td>KonturCrmApiKey</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
    </tr>
    <tr>
        <td>SentryDsn*</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
        <td>пустое значение</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
        <td>боевой Контура</td>
    </tr>
    <tr>
        <td>ZorroApiKey*</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
    </tr>
    <tr>
        <td>BBPortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <td>CAPortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <td>CCPortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <td>DSPortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <td>PSPortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <td>RCPortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <td>TSPortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <td>WSPortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <td>SupervisorServicePortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <td>TaskServicePortalApiKey*</td>
        <td>тестовый</td>
        <td>тестовый</td>
        <td>боевой Контура</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>боевой Ямала</td>
    </tr>
    <tr>
        <th>Секрет RA</th>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>RAOpenIdConnectApiKey</td>
        <td colspan="2" rowspan="5">нет секрета</td>
        <td>+</td>
        <td>+</td>
        <td colspan="2" rowspan="5">нет секрета</td>
    </tr>
    <tr>
        <td>SpanAggregationApiKey</td>
        <td>+</td>
        <td>+</td>
    </tr>
    <tr>
        <td>HerculesApiKey</td>
        <td>+</td>
        <td>-</td>
    </tr>
    <tr>
        <td>ELKApiKey</td>
        <td>боевой Контура</td>
        <td>пустое значение</td>
    </tr>
    <tr>
        <td>SentryDsn*</td>
        <td>боевой Контура</td>
        <td>пустое значение</td>
    </tr>
</table>

\* — обязательно наличие ключа, без него кабинет развалится  
** — необходимы отдельные доступы с АРМов в Vault со стороны партнера